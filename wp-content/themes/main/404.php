<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 */

get_header(); ?>
<section class="error-page error-page__404">
    <h1 class="error-page__title">Error 404</h1>
    
    <div class="error-page__image">
        <?php svg_paste("404"); ?>
    </div>
    
    <p class="error-page__description"><?php _e("It looks like nothing was found at this location. Maybe try a search?", "main"); ?></p>
    
    <?php get_search_form(); ?>
</section>
<?php get_footer(); ?>