<?php
/**
 * The template for displaying 403 pages (Forbidden)
 *
 */

get_header(); ?>
<section class="error-page error-page__403">
    <h1 class="error-page__title">Error 403</h1>
    
    <div class="error-page__image">
        <?php svg_paste("403"); ?>
    </div>
    
    <p class="error-page__description"><?php _e("We are sorry, but you do not have access to this page", "main"); ?></p>
</section>
<?php get_footer(); ?>