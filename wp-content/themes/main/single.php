<?php
/**
* Post Template
*/
 
get_header();

$video_field = get_field("video_url");
$subtitle_field = get_field("subtitle");
?>

<section class="post-detail">
    <div class="post-detail__wrapper">
        <div class="post-detail__content">
            <div class="post-detail__top">
                <?php
                    $categories = get_the_category($post->ID);
                    $cat_link = get_category_link($categories[0]->term_id);
                ?>
                <a class="post-detail__category" href="<?php print $cat_link; ?>">
                    <?php print esc_html($categories[0]->name); ?>
                </a>
                
                <h1 class="post-detail__title"><?php the_title(); ?></h1>

                <?php if ($subtitle_field) : ?>
                    <p class="post-detail__description">
                        <?php print $subtitle_field; ?>
                    </p>
                <?php endif; ?>

                <time class="post-detail__date">
                    <?php print get_the_date(); ?>
                </time>

                <div class="social-sharing">
                    <a href="" class="social-sharing__facebook" target="_blank" title="Facebook"></a>
                    <a href="https://twitter.com/intent/tweet?url=<?php print esc_url(get_permalink()); ?>" rel="noopener" class="social-sharing__twitter" target="_blank" title="Twitter"></a>
                    <a href="whatsapp://send?text=<?php the_title(); ?>" data-action="share/whatsapp/share" class="social-sharing__whatsapp" target="_blank" title="Whatsapp"></a>
                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php the_excerpt(); ?>&source=News" class="social-sharing__linkedin" target="_blank" title="Linkedin"></a>
                </div>
            </div>

            <figure class="post-detail__image">
                <?php if ($video_field) : ?>
                    <div class="post-detail__video-play video-embed" data-video="<?php print $video_field; ?>">
                        <?php svg_paste("play_white"); ?>
                    </div>
                <?php endif; ?>

                <?php the_post_thumbnail("detail-thumbnail", array('class' => "post-detail__image-img")); ?>
            </figure>

            <div class="post-detail__body">
                <?php the_content(); ?>

                <div class="post-detail__tags">
                    <?php the_tags("<span class='post-detail__tag-item'>", "</span><span class='post-detail__tag-item'>", "</span>"); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_template_part( "partials/post/content", "related-posts" );

get_footer();