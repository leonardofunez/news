<?php
/**
 * The template for displaying the footer.
 *
 */
?>
		</main>
	</div>
		<?php wp_footer(); ?>
		<footer class="footer">
			<div class="wrapper">
				<a href="<?php print home_url(); ?>" class="footer__logo logo"><?php svg_paste("logo_white"); ?></a>

				<?php wp_nav_menu( array( "theme_location" => "footer" ) ); ?>

				<div class="footer__author">
					Designed on <a href="https://www.figma.com" target="_blank">Figma</a> and built on <a href="https://wordpress.com/" target="_blank">WordPress</a> with <span>❤</span> by <a href="https://www.leonardofunez.com" title="Leonardo Funez">Leonardo Funez</a>
				</div>
			</div>
		</footer>
</body>
</html>