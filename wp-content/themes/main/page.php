<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>
	<section class="page-detail">
		<div class="wrapper">
			<h1 class="page-detail__title">
				<?php the_title(); ?>
			</h1>
			<?php while (have_posts()) : the_post(); ?>
				<div class="page-detail__body">
					<?php the_content(); ?>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php
get_footer();