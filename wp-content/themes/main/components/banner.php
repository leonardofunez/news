<?php
    namespace components\banner;
    use components\button\Button;

    class Banner {
        private $image;
        private $title;
        private $desc;
        private $cta_text;
        private $cta_link;
        private $type;

        public function __construct( $banner_data ) {
            $this->image    = isset($banner_data["image"])    ? $banner_data["image"]    : null;
            $this->title    = isset($banner_data["title"])    ? $banner_data["title"]    : null;
            $this->desc     = isset($banner_data["desc"])     ? $banner_data["desc"]     : null;
            $this->cta_text = isset($banner_data["cta_text"]) ? $banner_data["cta_text"] : null;
            $this->cta_link = isset($banner_data["cta_link"]) ? $banner_data["cta_link"] : null;
            $this->type     = isset($banner_data["type"])     ? $banner_data["type"]     : null;
        }

        public function render() {
            $is_horizonal = $this->type === "horizonal" ? true : false;

            print $is_horizonal ? "
                    <section class='banner-block' style='background-image: url($this->image);'>
                        <div class='banner-block__content'>".
                            ($this->title ? "<p class='banner-block__text'>".$this->title."</p>" : "").
                            
                            ($this->cta_text ? "<button class='button cta-modal'>".$this->cta_text."</button>" : "")."
                        </div>
                    </section>
                " : "
                <section class='promo-block'>
                    <div class='promo-block__bg'></div>

                    <div class='promo-block__content'>".
                        ($this->title ? "<h2 class='promo-block__title'>".$this->title."</h2>" : "")."

                        <div class='promo-block__bottom'>".
                            ($this->desc ? "<p class='promo-block__text'>".$this->desc."</p>" : "").

                            ($this->cta_text ? "<button class='button cta-modal'>".$this->cta_text."</button>" : "")."
                        </div>        
                    </div>
                </section>
            ";
        }
    };
?>