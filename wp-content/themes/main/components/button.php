<?php
    namespace components\button;

    class Button {
        private $text;
        private $link;
        private $is_modal;
        private $cta_modal_type;
        private $video_url;
        private $is_rounded;
        private $is_transparent;
        
        public function __construct( $button_data ) {
            $this->text           = isset($button_data["text"])           ? $button_data["text"]           : null;
            $this->link           = isset($button_data["link"])           ? $button_data["link"]           : null;
            $this->video_url      = isset($button_data["video_url"])      ? $button_data["video_url"]      : null;
            $this->is_rounded     = isset($button_data["is_rounded"])     ? $button_data["is_rounded"]     : false;
            $this->is_transparent = isset($button_data["is_transparent"]) ? $button_data["is_transparent"] : false;
            $this->cta_modal_type = isset($button_data["cta_modal_type"]) ? $button_data["cta_modal_type"] : false;
        }

        public function render() {
            return $this->text ? 
                "<button 
                    class='".
                        ($this->is_rounded ? "button-rounded" : "button").
                        ($this->is_transparent ? " button-rounded--transparent" : "").
                        ($this->cta_modal_type == "video" ? " cta-modal-video" : ($this->cta_modal_type == "form" ? " cta-modal" : ""))
                    ."' 
                    data-video='".$this->video_url."'
                    onclick='".($this->link ? "location.href='".$this->link."'" : "")."'
                >".
                    $this->text
                ."</button>"
            :
                null
            ;
        }
    };
?>