<?php
    namespace components\postCard;

    class PostCard {
        private $thumbnail;
        private $category;
        private $cat_link;
        private $title;
        private $date;
        private $link;
        private $desc;
        private $video;
        private $author;
        private $css_class;

        public function __construct( $post_data ) {
            $this->thumbnail = isset($post_data["thumbnail"]) ? $post_data["thumbnail"] : null;
            $this->category  = $post_data["category"];
            $this->cat_link  = $post_data["cat_link"];
            $this->title     = $post_data["title"];
            $this->date      = $post_data["date"];
            $this->link      = $post_data["link"];
            $this->desc      = isset($post_data["desc"]) ? $post_data["desc"] : null;
            $this->video     = isset($post_data["video"]) ? $post_data["video"] : null;
            $this->css_class = isset($post_data["css_class"]) ? $post_data["css_class"] : null;
        }

        public function render() {
            print "
            <article class='post-card ".$this->css_class."'>".
                ($this->thumbnail ? "
                <a class='post-card__thumbnail' href='".$this->link."'>".
                    ($this->video ? "<div class='post-card__play-button'></div>" : "").
                    "<figure class='post-card__thumbnail-bg' style='background: url(".$this->thumbnail.") no-repeat center / cover;'></figure>
                </a>" : "")."

                <div class='post-card__info'>
                    <a class='post-card__category' href='".$this->cat_link."'>".$this->category."</a>
                    <h2 class='post-card__title'>
                        <a href='".$this->link."'>".$this->title."</a>
                    </h2>
                </div>
            </article>";
        }
    };
?>