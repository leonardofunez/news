<?php
// **** Import class components **** //
	require_once( __DIR__ . '/components/post-card.php');
	use components\postCard\PostCard;
// **** //


// **** Load CSS and JS files **** //
    function add_google_fonts() {
        wp_enqueue_style("add-google-fonts", "https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600&family=Oswald:wght@400;500;600&display=swap", array(), null);
    }
    add_action("wp_enqueue_scripts", "add_google_fonts");
	
    function news_css_styles() {
        wp_enqueue_style("normalize", get_theme_file_uri("/assets/css/normalize.css"));
		wp_enqueue_style("main", get_theme_file_uri("/assets/css/main.css"));
	}
	add_action("wp_enqueue_scripts", "news_css_styles");

	function news_js_files() {
		wp_enqueue_script("jquery", get_theme_file_uri("/assets/js/jquery.js"), array(),"3.6.0", true);
		wp_enqueue_script("main", get_theme_file_uri("/assets/js/main.js"));
	}
	add_action("wp_enqueue_scripts", "news_js_files");
// **** //


// **** Register Menus **** //
	function register_menus() {
		register_nav_menus(
			array(
				"header" => __("Header"),
				"footer" => __("Footer"),
			)
		);
	}
	add_action("init", "register_menus");
// **** //

// **** Get JSON Menu **** //
	function json_header_menu() {
		return wp_get_nav_menu_items("header");
	}

	function json_footer_menu() {
		return wp_get_nav_menu_items("footer");
	}

	add_action("rest_api_init", function() {
		register_rest_route("wp/v2","/menu-header", array(
			"methods" => "GET",
			"callback" => "json_header_menu",
			"permission_callback" => "__return_true"
		));

		register_rest_route("wp/v2","/menu-footer", array(
			"methods" => "GET",
			"callback" => "json_footer_menu",
			"permission_callback" => "__return_true"
		));

		register_rest_field("search-result", "excerpt", array(
			"get_callback" => function ( $post_arr ) {
				return $post_arr["excerpt"];
			},
		));
	});
// **** //

// **** Add ACF in Post REST API **** //
	function create_ACF_meta_in_REST() {
		$postypes_to_exclude = ["acf-field-group","acf-field"];
		$extra_postypes_to_include = ["page", "post"];
		$post_types = array_diff(get_post_types(["_builtin" => false], "names"), $postypes_to_exclude);

		array_push($post_types, $extra_postypes_to_include);

		foreach ($post_types as $post_type) {
			register_rest_field( $post_type, "ACF", [
				"get_callback"    => "expose_ACF_fields",
				"schema"          => null,
			]);
		}
	}

	function expose_ACF_fields( $object ) {
		$ID = $object['id'];
		return get_fields($ID);
	}

	add_action("rest_api_init", "create_ACF_meta_in_REST");
// **** //

// **** Thumbnails **** //
	add_theme_support("post-thumbnails");

	add_image_size("detail-thumbnail",   1420, 700, true);
	add_image_size("featured-thumbnail", 768,  410, true);
	add_image_size("large-thumbnail",    444,  247, true);
	add_image_size("medium-thumbnail",   338,  194, true);
	add_image_size("small-thumbnail",    194,  122, true);
// **** //


// **** ACF Global **** //
	if (function_exists("acf_add_options_page")) { 
		acf_add_options_page(array(
			"page_title"    => "Theme Options",
			"menu_title"    => "Theme Options",
			"menu_slug"     => "theme-options",
			"capability"    => "edit_posts"
		)); 
	}
// **** //


// **** Add HTML5 theme support **** //
	function add_html5_support_for_search_form() {
		add_theme_support("html5", array("search-form"));
	}
	add_action("after_setup_theme", "add_html5_support_for_search_form");
// **** //


// **** SVG Render **** //
	function svg_paste($svgName, $returnType = false) {
		$svg_content = @file_get_contents(get_template_directory() . "/assets/images/" . $svgName . ".svg");
		if ($returnType) {
			return $svg_content;
		} else {
			print $svg_content;
		}
	}
// **** //
 

// **** Pick out the version number from scripts and styles **** //
	function remove_version_from_style_js( $src ) {
		if (strpos( $src, "ver=" . get_bloginfo( "version" )) )
		$src = remove_query_arg( "ver", $src );
		return $src;
	}
	add_filter("style_loader_src", "remove_version_from_style_js");
	add_filter("script_loader_src", "remove_version_from_style_js");
// **** //


// **** Load More Posts **** //
	add_action("wp_ajax_nopriv_load_more_posts", "load_more_posts");
	add_action("wp_ajax_load_more_posts", "load_more_posts");
	
	function load_more_posts() {
		$next_page = $_GET["current_page"];
		$category = $_GET["category_name"];

		$query = new WP_Query([
			"posts_per_page" => 8,
			"paged" => $next_page,
			"category_name" => $category
		]);
		
		if ($query->have_posts()):
			ob_start();

			while ($query->have_posts()): $query->the_post();
				$categories = get_the_category();

				$post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), "medium-thumbnail");
				$post_category  = $categories[0]->name;
				$post_cat_link  = get_category_link($categories[0]->term_id);
				$post_title     = get_the_title();
				$post_date      = get_the_date();
				$post_link      = get_the_permalink();
				$post_video     = get_field("video_url");

				$post_data = array(
					"thumbnail" => $post_thumbnail,
					"category"  => $post_category,
					"cat_link"  => $post_cat_link,
					"title"     => $post_title,
					"date"      => $post_date,
					"link"      => $post_link,
					"video"     => $post_video,
					"css_class" => "post-card--medium"
				);
				
				$POSTCARD = new PostCard($post_data);
				$POSTCARD->render();
			endwhile;
			
			$next_page++;

			wp_send_json_success(ob_get_clean());
		else:
			wp_send_json_error(false);
		endif;

		die;
	}
// **** //