<?php
    use components\postCard\PostCard;
    
    global $post;
    $post_category = get_the_category( $post->ID );
    
    $related_posts = new WP_Query(
        array(
            "post_type"      => "post",
            "posts_per_page" => 4,
            "post_status"    => "publish",
            "cat"            => $post_category[0]->term_id
        )
    );
?>

<section class="related-posts">
    <h3 class="related-posts__title">Related posts</h3>
    
    <div class="list-block list-block--medium">
        <?php
            if( $related_posts->have_posts() ):
                $post_count = 0;

                while( $related_posts->have_posts() ):
                    $related_posts->the_post();

                    $categories = get_the_category();
                    
                    $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), "full");
                    $post_category  = $categories[0]->name;
                    $post_cat_link  = get_category_link($categories[0]->term_id);
                    $post_title     = get_the_title();
                    $post_desc      = get_the_excerpt();
                    $post_date      = get_the_date();
                    $post_link      = get_the_permalink();
                    $post_video     = get_field("video_url");

                    $post_data = array(
                        "thumbnail" => $post_thumbnail,
                        "category"  => $post_category,
                        "cat_link"  => $post_cat_link,
                        "title"     => $post_title,
                        "date"      => $post_date,
                        "link"      => $post_link,
                        "desc"      => $post_desc,
                        "video"     => $post_video,
                        "css_class" => "post-card--medium"
                    );

                    $POSTCARD = new PostCard($post_data);
                    $POSTCARD->render();

                    $post_count++;
                endwhile;
                wp_reset_postdata();
            endif;
        ?>
    </div>
</section>