<?php
    $most_read_query = new WP_Query(
        array(
            "post_type"      => "post",
            "posts_per_page" => 9,
            "post_status"    => "publish"
        )
    );
?>
<section class="most-read">
    <h2 class="section-title"><?php _e("Trending"); ?></h2>
    
    <div class="most-read__posts">
        <?php
            if( $most_read_query->have_posts() ):
                $post_count = 1;

                while( $most_read_query->have_posts() ):
                    $most_read_query->the_post();

                    $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), "small-thumbnail");
                    $post_title     = get_the_title();
                    $post_link      = get_the_permalink(); ?>
                    
                    <article class="post-card-number">
                        <span class="post-card-number__index"><?php print $post_count; ?></span>
                        <h2 class="post-card-number__title">
                            <a href="<?php print $post_link; ?>"><?php print $post_title ; ?></a>
                        </h2>
                        <a href="<?php print $post_link; ?>">
                            <figure class="post-card-number__image" style="background: url(<?php print $post_thumbnail; ?>) no-repeat center / cover;"></figure>
                        </a>
                    </article>
                    
                    <?php
                    $post_count++;
                endwhile;
                wp_reset_postdata();
            endif;
        ?>
    </div>
</section>