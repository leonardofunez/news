<?php
    use components\banner\Banner;

    $homepageID             = 2;
    $horizontal_text        = get_post_field("horizontal_text", $homepageID);
    $horizontal_cta_text    = get_post_field("horizontal_cta_text", $homepageID);
    $horizontal_image_field = get_post_field("horizontal_image", $homepageID);
    $horizontal_image       = wp_get_attachment_image_src($horizontal_image_field, "full")[0];

    $banner_data = array (
        "image"    => $horizontal_image,
        "title"    => $horizontal_text,
        "cta_text" => $horizontal_cta_text,
        "type"     => "horizonal"
    );
    $BANNER = new Banner($banner_data);
    $BANNER->render();
?>