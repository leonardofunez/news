<?php
    use components\postCard\PostCard;
    
    $category_url = get_the_category();
    $category_filter = isset($_POST["category-filter"]) ? $_POST["category-filter"] : null;
    $category_slug = isset($category_filter) ? $category_filter : $category_url[0]->slug;
?>

<section class="posts-block">
    <div class="wrapper">
        <!-- Filter -->
            <div class="filter-container">
                <form method="post" name="filter-categories">
                    <select name="category-filter" class="dropdown" onchange="this.form.submit();">
                        <option value="">Category filter</option>
                        <?php
                            $cat_filter_args = array(
                                "orderby" => "name",
                                "order" => "ASC"
                            );

                            $categories = get_categories($cat_filter_args);

                            foreach($categories as $category): ?>
                                <option value="<?php print $category->slug; ?>">
                                    <?php print $category->name; ?>
                                </option>
                                <?php
                            endforeach;
                        ?>
                    </select>
                </form>
            </div>
        <!-- .Filter -->

        <!-- Posts List -->
            <div class="posts-block__list posts-block__list--small">
                <?php
                    $category_args = array(
                        "posts_per_page" => -1,
                        "paged" => get_query_var("paged"),
                        "category_name" => $category_slug
                    );

                    $category_posts = new WP_Query($category_args);

                    while ( $category_posts->have_posts() ) : $category_posts->the_post();
                        $categories = get_the_category();

                        $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), "full");
                        $post_category  = $categories[0]->name;
                        $post_title     = get_the_title();
                        $post_desc      = get_the_excerpt();
                        $post_date      = get_the_date();
                        $post_link      = get_the_permalink();
                        $post_video     = get_field("video_url");
                        $post_author    = get_the_author();

                        $post_data = array (
                            "thumbnail" => $post_thumbnail,
                            "category"  => $post_category,
                            "title"     => $post_title,
                            "desc"      => $post_desc,
                            "date"      => $post_date,
                            "link"      => $post_link,
                            "video"     => $post_video,
                            "author"    => $post_author,
                            "type"      => "post-card__small"
                        );
                        
                        $postCard = new PostCard($post_data);
                        $postCard->render(); 
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        <!-- .Posts List -->
        
        <!-- Pagination -->
            <div class="pagination">
                <div class="pagination__item">
                    <?php previous_posts_link("« Newer entries"); ?>
                </div>

                <div class="pagination__item">
                    <?php next_posts_link("Older entries »", $category_posts->max_num_pages); ?>
                </div>
            </div>
        <!-- .Pagination -->
    </div>
</section>