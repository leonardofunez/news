<?php
    use components\banner\Banner;

    $homepageID           = 2;
    $vertical_title       = get_post_field("veritical_title", $homepageID);
    $vertical_text        = get_post_field("veritical_text", $homepageID);
    $vertical_cta_text    = get_post_field("veritical_cta_text", $homepageID);
    $vertical_image_field = get_post_field("vertical_image", $homepageID);
    $vertical_image       = wp_get_attachment_image_src($vertical_image_field, "full")[0];

    $banner_data = array (
        "image"    => $vertical_image,
        "title"    => $vertical_title,
        "desc"     => $vertical_text,
        "cta_text" => $vertical_cta_text
    );
    $BANNER = new Banner($banner_data);
    $BANNER->render();
?>