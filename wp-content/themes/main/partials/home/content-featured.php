<?php
    use components\postCard\PostCard;

    $featured_query = new WP_Query(
        array(
            "post_type"      => "post",
            "posts_per_page" => 3,
            "post_status"    => "publish"
        )
    );
?>

<section class="featured-block">
    <?php
        if( $featured_query->have_posts() ):
            $post_count = 0;

            while( $featured_query->have_posts() ):
                $featured_query->the_post();

                $categories = get_the_category();
                
                $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), "featured-thumbnail");
                $post_category  = $categories[0]->name;
                $post_cat_link  = get_category_link($categories[0]->term_id);
                $post_title     = get_the_title();
                $post_desc      = get_the_excerpt();
                $post_date      = get_the_date();
                $post_link      = get_the_permalink();
                $post_video     = get_field("video_url");

                $post_data = array(
                    "thumbnail" => $post_thumbnail,
                    "category"  => $post_category,
                    "cat_link"  => $post_cat_link,
                    "title"     => $post_title,
                    "date"      => $post_date,
                    "link"      => $post_link,
                    "desc"      => $post_desc,
                    "video"     => $post_video,
                    "css_class" => "post-card--featured ".($post_count == 0 ? "post-card--featured-main" : "post-card--featured-list")
                );

                $POSTCARD = new PostCard($post_data);
                $POSTCARD->render();

                $post_count++;
            endwhile;
            wp_reset_postdata();
        endif;
    ?>
</section>