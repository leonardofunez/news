<?php
    use components\postCard\PostCard;

    $query_posts = new WP_Query(
        array(
            "post_type" => "post",
            "posts_per_page" => 1,
            "post_status" => "publish"
        )
    );
?>

<section class="posts-block posts-block--featured">
    <div class="wrapper">
        <div class="posts-block__list posts-block__list--featured">
            <?php
                if( $query_posts->have_posts() ):
                    while( $query_posts->have_posts() ):
                        $query_posts->the_post();

                        $categories = get_the_category();
                        $cat_link = get_category_link($categories[0]->term_id);
                        
                        $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), "full");
                        $post_category  = $categories[0]->name;
                        $post_title     = get_the_title();
                        $post_desc      = get_the_excerpt();
                        $post_date      = get_the_date();
                        $post_link      = get_the_permalink();
                        $post_video     = get_field("video_url");
                        $post_author    = get_the_author();
                        
                        $post_data = array (
                            "thumbnail" => $post_thumbnail,
                            "category"  => $post_category,
                            "title"     => $post_title,
                            "desc"      => $post_desc,
                            "date"      => $post_date,
                            "link"      => $post_link,
                            "video"     => $post_video,
                            "author"    => $post_author,
                            "type"      => "featured"
                        );
                        
                        $POST_CARD = new PostCard($post_data);
                        $POST_CARD->render(); 
                    endwhile;
                    wp_reset_postdata();
                endif;
            ?>
        </div>

        <?php get_template_part( "partials/content", "promo" ); ?>
    </div>
</section>