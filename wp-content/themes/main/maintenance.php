<?php
/**
 * The template for maintenance page
 *
 */

get_header(); ?>
<section class="error-page error-page__maintenance">
    <h1 class="error-page__title">Maintenance Page</h1>
    
    <div class="error-page__image">
        <?php svg_paste("maitenance"); ?>
    </div>
    
    <p class="error-page__description"><?php _e("We are preparing to serve you better.", "main"); ?></p>
</section>
<?php get_footer(); ?>