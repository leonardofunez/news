let gulp = require('gulp');
let babel = require('gulp-babel');
let concat = require("gulp-concat");
let minify = require('gulp-minify');
let sass = require('gulp-sass');
let postcss = require('gulp-postcss');
let flexbugs = require('postcss-flexbugs-fixes');
let autoprefixer = require('autoprefixer');
let sourcemaps = require('gulp-sourcemaps');

let SOURCE_DIR = '../scss/**/[^_]*.scss';
let ALL_SASS_FILES = '../scss/**/*.scss';
let ALL_JS_FILES = '../js/*.js';
let DEST_JS_DIR = '../js/';
let DEST_CSS_DIR = '../css/';

let autoprefixerBrowsers = [
	'last 2 versions',
	'ie 11',
	'> 2%'
];

gulp.task('compile_scss', () => {
	console.log("Compiling SCSS");

	let plugins = [
		autoprefixer({
			browsers: autoprefixerBrowsers
		}),
		flexbugs()
	];

	return gulp.src(SOURCE_DIR)
		.pipe(sourcemaps.init())
		.pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
		.pipe(postcss(plugins))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(() => DEST_CSS_DIR));
});

gulp.task('compile_scripts', () => {
	console.log("Compiling Scripts");

	return gulp.src(ALL_JS_FILES)
      	.pipe(babel({presets: ['@babel/preset-env']}))
		.pipe(concat("main.js"))
		.pipe(minify())
		.pipe(gulp.dest(() => "../es5"));
});

gulp.task('watch', function () {
	console.log("Starting the watcher");
	gulp.watch(ALL_SASS_FILES, gulp.series('compile_scss'));
	gulp.watch(ALL_JS_FILES, gulp.series('compile_scripts'));
});

gulp.task('default', gulp.parallel('compile_scss', 'compile_scripts', 'watch'));