const path = require("path");

const jsRules = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: "babel-loader",
        options: {
            presets: ["@babel/preset-env"],
            plugins: ["@babel/plugin-proposal-optional-chaining"]
        }
    }
};

const cssRules = {
    test: /\.(s(a|c)ss)$/,
    use: [
        {
            loader: "file-loader",
            options: {
                name: "../css/main.css"
            },
        },
        {
            loader: "sass-loader"
        }
    ]
};

module.exports = {
    entry: {
        main:[
            path.resolve("../js/index.js"),
            path.resolve("../scss/main.scss")
        ]
    },
    output: {
        path: path.resolve("../js/"),
        filename: "main.js"
    },
    module: {
        rules: [jsRules, cssRules]
    }
}