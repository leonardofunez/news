const fetchPosts = (category, current_page, max_pages) => {
    const response = fetch(`/news/wp-admin/admin-ajax.php?action=load_more_posts&category_name=${category}&current_page=${current_page}&max_pages=${max_pages}`, {
        method: "post",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    })
    .then(response => response.json())
    .then(data => {
        return data.data;
    });
    
    return response || [];
};

const ajaxLoadMore = () => {
    if (document?.body?.classList.contains("category")) {
        const $categoryPage = document.querySelector(".category-page");
        const $loadMoreBtn = document.querySelector(".button__load-more");
        const $listPosts = document.querySelector(".list-block");
        
        let current_page = $listPosts.dataset.page || 1;
        let max_pages = $listPosts.dataset.max;
        let category = $listPosts.dataset.category;

        const renderPosts = async () => {
            const posts = await fetchPosts(category, current_page, max_pages);
            posts ? ($listPosts.innerHTML += posts) : $loadMoreBtn.classList.add("button--hidden");
            $categoryPage.classList.remove("category-page--is-loading");
            $listPosts.dataset.page++;

            setTimeout(() => {
                $loadMoreBtn.classList.remove("button--loading");
            }, 200);
        };
        
        window.addEventListener("load", () => {
            if (typeof $loadMoreBtn !== "undefined" && $loadMoreBtn !== null) {
                renderPosts();

                $loadMoreBtn.addEventListener("click", (e) => {
                    if (!$loadMoreBtn.classList.contains("button--loading")) {
                        current_page = $listPosts.dataset.page;
                        max_pages = $listPosts.dataset.max;
                        category = $listPosts.dataset.category;

                        $loadMoreBtn.classList.add("button--loading");
                        
                        renderPosts();
                    }
                });
            }
        });
    }
};

export default ajaxLoadMore;