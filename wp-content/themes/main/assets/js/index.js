import "regenerator-runtime/runtime";
import ajaxLoadMore from "./ajax_load_more";

HTMLDocument.prototype.ready = function () {
    return new Promise(function(resolve, reject) {
        if (document.readyState === "complete") {
            resolve(document);
        } else {
            document.addEventListener("DOMContentLoaded", function() {
                resolve(document);
            });
        }
    });
};

document.ready().then(() => {
    ajaxLoadMore();
});

jQuery(document).ready(function( $ ) {
    // ******** MENU ******** //
    var $menuButton = $(".menu-button");
    var $menuHeader = $(".menu-header-container");
    
    $menuButton.on("click", function () {
        if ($(this).hasClass("menu-button--active")) {
            $(this).removeClass("menu-button--active");
            $menuHeader.removeClass("menu-header-container--active");
        } else {
            $(this).addClass("menu-button--active");
            $menuHeader.addClass("menu-header-container--active");
        }
    });
    // ****** END MENU ****** //

    // ******** DARKMODE ******* //
    var $darkModeTrigger = $(".dark-mode__trigger");

    if (localStorage.getItem("news__dark") === null) {
        localStorage.setItem("news__dark", false);
    } else {
        if (localStorage.getItem("news__dark") === "true") {
            $darkModeTrigger.addClass("dark-mode__trigger--is-on");
            $("body").addClass("dark-mode");
        } else {
            $darkModeTrigger.removeClass("dark-mode__trigger--is-on");
            $("body").removeClass("dark-mode");
        }
    }
    
    $darkModeTrigger.on("click", function() {
        if ($(this).hasClass("dark-mode__trigger--is-on")) {
            $(this).removeClass("dark-mode__trigger--is-on");
            $("body").removeClass("dark-mode");
            localStorage.setItem("news__dark", false);
        } else {
            $(this).addClass("dark-mode__trigger--is-on");
            $("body").addClass("dark-mode");
            
            localStorage.setItem("news__dark", true);
        };
    });
    // ******** END DARKMODE ******* //

    // ********** VIDEO ************ //
    var $videoEmbed = $(".video-embed");
    var $postDetailImage = $(".post-detail__image");
    
    function getVideoEmbedId(video_url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
        const match = video_url.match(regExp);

        return (match && match[2].length === 11) ? match[2] : null;
    }

    $videoEmbed.on("click", function(){
        var dataVideo = $(this).attr("data-video");
        var videoID = getVideoEmbedId(dataVideo);

        if (dataVideo) {
            const iframeMarkup = "<iframe class='video-embed__iframe' src='//www.youtube.com/embed/" + videoID + "' frameborder='0' allowfullscreen></iframe>";
            $postDetailImage.append(iframeMarkup);
        }
    });     
    // *********** END VIDEO *********** //
});