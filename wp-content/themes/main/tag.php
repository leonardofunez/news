<?php
/**
* Tag Template
*/
 
get_header();

use components\postCard\PostCard;

$tag = get_queried_object();
?>

<section class="category-page">
    <h1 class="category-page__name"><?php print $tag->name; ?></h1>

    <div class="list-block list-block--medium">
        <?php
            
            $tag_args = array(
                "posts_per_page" => -1,
                "tag" => $tag->slug
            );

            $tag_posts = new WP_Query($tag_args);
            $post_count = 0;

            while ( $tag_posts->have_posts() ) : $tag_posts->the_post();
                $categories = get_the_category();

                $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), "medium-thumbnail");
                $post_category  = $categories[0]->name;
                $post_cat_link  = get_category_link($categories[0]->term_id);
                $post_title     = get_the_title();
                $post_date      = get_the_date();
                $post_link      = get_the_permalink();
                $post_video     = get_field("video_url");

                $post_data = array(
                    "thumbnail" => $post_thumbnail,
                    "category"  => $post_category,
                    "cat_link"  => $post_cat_link,
                    "title"     => $post_title,
                    "date"      => $post_date,
                    "link"      => $post_link,
                    "video"     => $post_video,
                    "css_class" => "post-card--medium"
                );

                $POSTCARD = new PostCard($post_data);
                $POSTCARD->render();

                $post_count++;
            endwhile;
            wp_reset_postdata();
        ?>
    </div>
</section>

<?php
get_footer();