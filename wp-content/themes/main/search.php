<?php
/*
Template Name: Search Page
*/

use components\postCard\PostCard;
get_header();
?>

<section class="category-page">
    <h2 class="category-page__name">Search</h2>

	<?php get_search_form(); ?>

	<div class="list-block list-block--medium">
		<?php
			$search_query = get_search_query();
			$args = array("post_type" => "post", "s" => $search_query , "showposts" => -1);

			$the_query = new WP_Query( $args );
			
			if ($the_query->have_posts()):
				while ($the_query->have_posts()): $the_query->the_post();
					$categories = get_the_category();
					
					$post_thumbnail = get_the_post_thumbnail_url(get_the_ID(), "medium-thumbnail");
					$post_category  = $categories[0]->name;
					$post_cat_link  = get_category_link($categories[0]->term_id);
					$post_title     = get_the_title();
					$post_date      = get_the_date();
					$post_link      = get_the_permalink();
					$post_video     = get_field("video_url");

					$post_data = array(
						"thumbnail" => $post_thumbnail,
						"category"  => $post_category,
						"cat_link"  => $post_cat_link,
						"title"     => $post_title,
						"date"      => $post_date,
						"link"      => $post_link,
						"video"     => $post_video,
						"css_class" => "post-card--medium"
					);

					$POSTCARD = new PostCard($post_data);
					$POSTCARD->render();
				endwhile;
			else:
				print "<div class='search-result__no-result'>No result found!</div>";
			endif;
		?>
	</div>
</section>

<?php
get_footer();