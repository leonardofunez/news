<?php
/**
 * The header for main theme
**/
?>
<!doctype html>

<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#0D2452">

    <title>
        <?php print get_bloginfo('name'); ?>
    </title>

    <!-- Icons & favicons -->
        <!-- For iPhone 6 Plus -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php print get_theme_file_uri("/assets/images/favicons/180.png"); ?>">
        <!-- For iPad retina touch icon -->
        <link rel="apple-touch-icon" sizes="167x167" href="<?php print get_theme_file_uri("/assets/images/favicons/167.png"); ?>">
        <!-- For iPad touch icon -->
        <link rel="apple-touch-icon" sizes="152x152" href="<?php print get_theme_file_uri("/assets/images/favicons/152.png"); ?>">
        <!-- For iPhone retina touch icon -->
        <link rel="apple-touch-icon" sizes="120x120" href="<?php print get_theme_file_uri("/assets/images/favicons/120.png"); ?>">
        <!-- For iPad home screen -->
        <link rel="apple-touch-icon" sizes="76x76" href="<?php print get_theme_file_uri("/assets/images/favicons/76.png"); ?>">
        
        <link rel="shortcut icon" sizes="57x57" href="<?php print get_theme_file_uri("/assets/images/favicons/57.png"); ?>">
        
        <!-- For most desktop browsers -->
        <link rel="shortcut icon" href="<?php print get_theme_file_uri("/assets/images/favicons/32.png"); ?>">
    <!-- .Icons & favicons -->
<?php wp_head(); ?>
</head>

<?php
    $hasDarkLayer = is_front_page() || is_home() || is_single();
?>

<body <?php body_class($hasDarkLayer ? "has-dark-layer" : ""); ?>>
    <?php $hasDarkLayer ? print "<div class='bg-layer'></div>" : ""; ?>
    
    <div class="main-wrapper">
        <header id="header" class="header">
            <a href="<?php print home_url(); ?>" class="header__logo logo">
                <?php svg_paste($hasDarkLayer ? "logo_white" : "logo_dark"); ?>
            </a>

            <?php wp_nav_menu( array( "theme_location" => "header" ) ); ?>

            <button class="dark-mode__trigger"></button>

            <a class="search-button <?php print $hasDarkLayer ? "search-button__white" : ""; ?>" title="Search posts" href="<?php print get_site_url()."/?s="; ?>">
                <?php svg_paste("search"); ?>
            </a>
            
            <a class="button-live" title="Live NOW" href="<?php print get_site_url()."/live-now"; ?>">
                Live <span class="button-live__now">now</span>
            </a>

            <button class="menu-button">
                <span class="menu-button__line"></span>
                <span class="menu-button__line"></span>
                <span class="menu-button__line"></span>
            </button>
        </header>

        <main class="main-content">