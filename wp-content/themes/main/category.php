<?php
/**
* Category Template
*/
 
get_header();
global $wp_query;
$category = get_queried_object();
$post_count = $category->category_count;
?>

<section class="category-page category-page--is-loading">
    <h1 class="category-page__name"><?php print $category->name; ?></h1>
    <div class="loading-posts">Loading posts...</div>
    <div class="list-block list-block--section list-block--medium" 
        data-page="<?php print get_query_var("paged") ? get_query_var("paged") : 1; ?>" 
        data-max="<?php print $wp_query->max_num_pages; ?>"
        data-category="<?php print $category->slug; ?>"
        data-posts="<?php print $post_count; ?>"
    >
    </div>

    <button class="button button__load-more <?php print ($post_count <= 8) ? "button--hidden" : ""; ?>"><?php _e("Load More"); ?></button>
</section>

<?php
get_footer();