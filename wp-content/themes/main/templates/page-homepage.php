<?php
/**
* Template Name: Homepage
*/

get_header();

get_template_part( "partials/home/content", "featured" );
get_template_part( "partials/home/content", "list-large" );
get_template_part( "partials/sections/content", "most-read" );
get_template_part( "partials/home/content", "list-medium" );
get_template_part( "partials/home/content", "list-small" );

get_footer();