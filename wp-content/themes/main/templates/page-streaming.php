<?php
/**
* Template Name: Streaming
*/

get_header(); ?>
	<section class="page-detail page-detail--streaming">
		<div class="wrapper">
			<h1 class="page-detail__title">
				<?php the_title(); ?>
			</h1>
			<?php while (have_posts()) : the_post(); ?>
				<div class="page-detail__body">
					<?php the_content(); ?>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php
get_footer();