<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'news' );

/** MySQL database username */
define( 'DB_USER', 'manager' );

/** MySQL database password */
define( 'DB_PASSWORD', 'manager123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'JpIe<01.[-e)/{),Bp(Er&Clzs&M<XQx2elwdzrV*LZBJG6,[M}5L@*%M&YXb ch' );
define( 'SECURE_AUTH_KEY',  'v~U#!*j;kF--KK4E%,!Tm&kpVj)Ju{N+kUvyz|QIwX}4sKaV7|@N] v`(n|os;,q' );
define( 'LOGGED_IN_KEY',    'D<3/K%lPSje%?GnB~RbkpAcVGSC;X0%-azeRN!i @+~3K.,_3&q`bS?JXL2{|}Mr' );
define( 'NONCE_KEY',        'GiooKaeW.,6H$`rkFkovHHvv/@`>aVhzh&-{X_]o#y7@}29b{dW<HGsKH!C<Q`7?' );
define( 'AUTH_SALT',        '~~X%Z1TPW;%ip%!&R4]:KW22TA5>l 0*!.T)Q#rU`PH7#]d6i?mN/l^RevS^Lyoy' );
define( 'SECURE_AUTH_SALT', 'PfAQ25r;UMT|MqR=Q}:MO26c~5>bdd9(x1<f-Z=N! Y>t}xxI%WDt{#):l)q8D<o' );
define( 'LOGGED_IN_SALT',   'bHOD ;ATUW9G.Ddw30KX|@FO-y0)]R,M-J[8B_K-u/VKSUL?`E4#46KIMqS?!rE1' );
define( 'NONCE_SALT',       'h27l6ju5nnxF8e!f5&Gre7>+s6]|[qkN>U!lkz$}AB5P,30cL^&*(x@xSq5%oiXC' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
